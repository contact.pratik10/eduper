from django.db import models
from attendance.models import ClassRoom

from authentication.models import User
from institute.models import Institute

# Create your models here.



class BlogPost(models.Model):
    # image
    # video
    title = models.CharField(max_length=500, null=True, blank=True)
    description = models.CharField(max_length=5000, null=True, blank=True)
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE, blank=True, null=True)
    classroom = models.ForeignKey(ClassRoom, on_delete=models.CASCADE, blank=True, null=True)
    TYPE_CHOICES = (
        ('Blog', 'Blog'),
        ('Notice', 'Notice'),
    )
    type =  models.CharField(max_length=100, choices=TYPE_CHOICES, blank=True, null =True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.title)

        
class BlogLike(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    blog = models.ForeignKey(BlogPost, on_delete=models.CASCADE, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.user)


class BlogComment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    blog = models.ForeignKey(BlogPost, on_delete=models.CASCADE, blank=True, null=True)
    comment = models.CharField(max_length=300, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.user)

