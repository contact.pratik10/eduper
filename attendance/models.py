from __future__ import division
from django.db import models
import datetime
from institute.models import Role, Institute, FinancialYear
from authentication.models import User
# Create your models here.

class ClassRoom(models.Model):
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE, blank=True, null=True)
    standard = models.CharField(max_length=255, blank=True, null =True)
    division = models.CharField(max_length=255, blank=True, null =True)
    branch = models.CharField(max_length=255, blank=True, null =True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.standard) + " " + "(" + str(self.division) + ")" + " " + "(" + str(self.branch) + ")"


class ClassRoomStudent(models.Model):
    student = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    classroom = models.ForeignKey(ClassRoom, on_delete=models.CASCADE, blank=True, null=True)
    financial_year = models.ForeignKey(FinancialYear, on_delete=models.CASCADE, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.student) + " " + "(" + str(self.classroom) + ")"


class Subject(models.Model):
    subject = models.CharField(max_length=255, blank=True, null =True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.subject)
        

class InstituteSubject(models.Model):
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE, blank=True, null=True)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, blank=True, null =True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.subject) + " " + "(" + str(self.institute) + ")"



class SubjectTeacher(models.Model):
    teacher = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    subject = models.ForeignKey(InstituteSubject, on_delete=models.CASCADE, blank=True, null =True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.teacher) + " " + "(" + str(self.subject) + ")"



class Lecture(models.Model):
    teacher = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, blank=True, null =True)
    classroom = models.ForeignKey(ClassRoom, on_delete=models.CASCADE, blank=True, null =True)
    financial_year = models.ForeignKey(FinancialYear, on_delete=models.CASCADE, blank=True, null =True)
    date = models.DateField(default=datetime.datetime.now, blank=True, null=True)
    start_time = models.TimeField(default=datetime.datetime.now, blank=True, null=True)
    end_time = models.TimeField(default=None, blank=True, null=True)
    notes = models.CharField(max_length=900, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.institute) + " " + "(" + str(self.lecturer) + ")" + " " + "(" + str(self.financial_year) + ")"



class SchoolAttendance(models.Model):
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    role = models.ForeignKey(Role, on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateField(default=datetime.datetime.now, blank=True, null=True)
    time = models.TimeField(default=datetime.datetime.now, blank=True, null=True)
    financial_year = models.ForeignKey(FinancialYear, on_delete=models.CASCADE, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.user) + " " + "(" + str(self.institute) + ")" + " " + "(" + str(self.financial_year) + ")"



class CollegeAttendance(models.Model):
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    lecture = models.ForeignKey(Lecture, on_delete=models.CASCADE, blank=True, null=True)
    role = models.ForeignKey(Role, on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateField(default=datetime.datetime.now, blank=True, null=True)
    time = models.TimeField(default=datetime.datetime.now, blank=True, null=True)
    financial_year = models.ForeignKey(FinancialYear, on_delete=models.CASCADE, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.user) + " " + "(" + str(self.institute) + ")" + " " + "(" + str(self.financial_year) + ")"



class Timetable(models.Model):
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE, blank=True, null=True)
    classroom = models.ForeignKey(ClassRoom, on_delete=models.CASCADE, blank=True, null =True)
    subject = models.ForeignKey(InstituteSubject, on_delete=models.CASCADE, blank=True, null =True)
    DAY_CHOICES = (
        ('Monday', 'Monday'),
        ('Tuesday', 'Tuesday'),
        ('Wednesday', 'Wednesday'),
        ('Thursday', 'Thursday'),
        ('Friday', 'Friday'),
        ('Saturday', 'Saturday')
    )
    day = models.CharField(max_length=100, choices=DAY_CHOICES, blank=True, null =True)
    from_time = models.TimeField(default=datetime.datetime.now, blank=True, null=True)
    to_time = models.TimeField(default=datetime.datetime.now, blank=True, null=True)

    def __str__(self):
        return str(self.day) + " " + "(" + str(self.subject) + ")"