import imp
from django.contrib import admin
from attendance.models import SchoolAttendance, CollegeAttendance, Subject, Lecture, ClassRoom, ClassRoomStudent, SubjectTeacher, InstituteSubject, Timetable
# Register your models here.

admin.site.register(SchoolAttendance)
admin.site.register(CollegeAttendance)
admin.site.register(Subject)
admin.site.register(Lecture)
admin.site.register(ClassRoom)
admin.site.register(ClassRoomStudent)
admin.site.register(SubjectTeacher)
admin.site.register(InstituteSubject)
admin.site.register(Timetable)
