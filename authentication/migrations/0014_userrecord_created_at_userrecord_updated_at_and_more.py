# Generated by Django 4.0.4 on 2022-05-03 20:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0013_userrecord_role'),
    ]

    operations = [
        migrations.AddField(
            model_name='userrecord',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='userrecord',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AlterField(
            model_name='userrecord',
            name='end_date',
            field=models.DateField(blank=True, default=None, null=True),
        ),
    ]
