from django.db import models
import datetime
from django.contrib.auth.models import AbstractUser

from institute.models import Role, Institute
# Create your models here.


class User(AbstractUser):
    first_name = models.CharField(max_length=255, blank=True, null =True)
    last_name = models.CharField(max_length=255, blank=True, null =True)
    username = models.CharField(max_length=255, unique=True, blank=True, null =True)
    contact_no = models.CharField(max_length=12, blank=True, null=True)
    email = models.EmailField(unique=True, blank=True, null =True)
    password = models.CharField(max_length=255, blank=True, null =True)
    date_of_birth = models.DateField(blank=True, null =True)
    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female'),
    )
    gender = models.CharField(max_length=10, choices=GENDER_CHOICES, blank=True, null =True)
    role = models.ForeignKey(Role, on_delete=models.CASCADE, blank=True, null =True)
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE, blank=True, null =True)
    STATUS_CHOICES = (
        ('Active', 'Active'),
        ('Inactive', 'Inactive'),
    )
    subscription_status = models.CharField(max_length=10, choices=STATUS_CHOICES, blank=True, null =True, default="Inactive")
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)
    def __str__(self):
        if str(self.username) == "admin":
            return str("Admin")
        else:
            return str(self.first_name) + ' ' + str(self.last_name) + ' (' + str(self.role) + ')'

class UserRecord(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE, blank=True, null =True)
    role = models.ForeignKey(Role, on_delete=models.CASCADE, blank=True, null =True)
    start_date = models.DateField(default=datetime.datetime.now, blank=True, null=True)
    end_date = models.DateField(default=None, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.user) + " " + "(" + str(self.institute) + ")"
