import imp
from tkinter import CASCADE
import datetime
from django.db import models


# Create your models here.

class Role(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.name)

class Institute(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    contact_no = models.CharField(max_length=12, blank=True, null=True)
    STATUS_CHOICES = (
        ('Active', 'Active'),
        ('Inactive', 'Inactive'),
    )
    subscription_status = models.CharField(max_length=10, choices=STATUS_CHOICES, blank=True, null =True, default="Inactive")
    TYPE_CHOICES = (
        ('School', 'School'),
        ('College', 'College'),
    )
    type = models.CharField(max_length=10, choices=TYPE_CHOICES, blank=True, null =True, default="Inactive")
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.name)

class FinancialYear(models.Model):
    financial_year = models.CharField(max_length=9, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.financial_year)
    

class Holiday(models.Model):
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE, blank=True, null =True)
    holiday_dates = models.JSONField()
    financial_year = models.ForeignKey(FinancialYear, on_delete=models.CASCADE, blank=True, null =True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, blank=True)

    def __str__(self):
        return str(self.institute) + ' ' + ' (' + str(self.financial_year) + ')'



