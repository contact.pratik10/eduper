# Generated by Django 4.0.4 on 2022-05-02 22:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('institute', '0005_rename_holidays_holiday'),
    ]

    operations = [
        migrations.AddField(
            model_name='financialyear',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='financialyear',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AddField(
            model_name='holiday',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='holiday',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AddField(
            model_name='role',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='role',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
    ]
