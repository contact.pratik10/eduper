from django.contrib import admin
from institute.models import Role, Institute, FinancialYear, Holiday
# Register your models here.
admin.site.register(Role)
admin.site.register(Institute)
admin.site.register(FinancialYear)
admin.site.register(Holiday)

